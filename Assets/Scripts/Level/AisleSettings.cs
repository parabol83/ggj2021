using System.Collections.Generic;
using UnityEngine;

namespace Level
{
    [CreateAssetMenu(fileName = "AisleSettings", menuName = "Level/Aisle Settings")]
    public class AisleSettings : ScriptableObject
    {
        public List<ItemDisplay> itemDisplays;
    }
}
