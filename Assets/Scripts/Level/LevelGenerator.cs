using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Level
{
    public class LevelGenerator : MonoBehaviour
    {
        private class RowData
        {
            public int aisleLength;
            public int rowWidth;
            public StoreZoneSettings[] zones;
            public Vector2Int[] zoneDimensions;

            public RowData(StoreRowSettings settings, bool allowZoneAisleLengths)
            {
                zones = settings.GetZoneList();
                zoneDimensions = new Vector2Int[zones.Length];

                rowWidth = 1 * (zones.Length - 1); //Allow for border paths.
                aisleLength = allowZoneAisleLengths ? 0 : Random.Range(settings.minAisleLength, settings.maxAisleLenght);

                for (var i = 0; i < zones.Length; i++)
                {
                    zoneDimensions[i] = zones[i].GetDimensions();
                    rowWidth += zoneDimensions[i].x;

                    if (allowZoneAisleLengths)
                    {
                        aisleLength = Math.Max(aisleLength, zoneDimensions[i].y);
                    }
                    else
                    {
                        zoneDimensions[i].y = aisleLength;
                    }
                }
            }
        }

        private class CellData
        {
            private static readonly CellData NullCellData = new CellData();

            private StoreZoneSettings zoneSettings;
            private AisleSettings leftAisleSettings;
            private AisleSettings rightAisleSettings;
            private ItemDisplay topAisleDisplay;
            private ItemDisplay bottomAisleDisplay;
            private Dictionary<ShoppingItem.ItemType, List<ShoppingItem>> leftAisleShoppingItems;
            private Dictionary<ShoppingItem.ItemType, List<ShoppingItem>> rightAisleShoppingItems;
            private bool isZoneTop;
            private bool isZoneBottom;
            private bool isZoneLeft;
            private bool isZoneRight;
            private bool isBorder;

            public CellData neighbourT = NullCellData;
            public CellData neighbourL = NullCellData;
            public CellData neighbourR = NullCellData;
            public CellData neighbourB = NullCellData;
            public readonly int x;
            public readonly int y;


            private CellData()
            {
                zoneSettings = null;
                leftAisleSettings = null;
                rightAisleSettings = null;

                isZoneTop = false;
                isZoneBottom = false;
                isZoneLeft = false;
                isZoneRight = false;
                isBorder = false;

                neighbourT = this;
                neighbourL = this;
                neighbourR = this;
                neighbourB = this;
                x = -1;
                y = -1;
            }

            public CellData(int xIndex, int yIndex)
            {
                x = xIndex;
                y = yIndex;
            }

            public void SetZoneData(StoreZoneSettings storeZoneSettings, bool top, bool bottom, bool left, bool right)
            {
                zoneSettings = storeZoneSettings;
                isZoneTop = top;
                isZoneBottom = bottom;
                isZoneLeft = left;
                isZoneRight = right;
            }

            public void UpdateVerticalBorders()
            {
                if (IsPopulated)
                {
                    return;
                }

                if (neighbourT.IsZoned && !neighbourT.IsBorder)
                {
                    isBorder = true;

                    if (!neighbourB.IsZoned || neighbourB.IsBorder)
                    {
                        SetZoneData(neighbourT.zoneSettings, false, true, neighbourT.isZoneLeft, neighbourT.isZoneRight);
                        neighbourT.isZoneBottom = false;
                    }
                }
                else if (neighbourB.IsZoned && !neighbourB.IsBorder)
                {
                    isBorder = true;
                    SetZoneData(neighbourB.zoneSettings, true, false, neighbourB.isZoneLeft, neighbourB.isZoneRight);
                    neighbourB.isZoneTop = false;
                }
            }

            public void UpdateHorizontalBorders()
            {
                if (IsPopulated)
                {
                    return;
                }

                if (neighbourL.IsPopulated && neighbourR.IsPopulated)
                {
                    isBorder = true;
                }
            }

            public bool IsPopulated => zoneSettings != null || isBorder;
            public bool IsZoned => zoneSettings != null;
            public bool IsBorder => isBorder;
            public bool IsZonedBorder => zoneSettings != null & isBorder;


            public void Instantiate(LevelSettings currentLevelSettings, Transform parentTransform)
            {
                if (zoneSettings == null && !isBorder)
                {
                    return;
                }

                var position = new Vector3(currentLevelSettings.AisleCellSize.x * x, 0f, currentLevelSettings.AisleCellSize.y * y);

                //GameObject floorObj = currentLevelSettings.BorderFloorTiles.PickRandomItem();

                var currentZoneSettings = zoneSettings ? zoneSettings : currentLevelSettings.borderZoneSettings;
                var floorTileList = currentZoneSettings.FloorSegements;

                if (isBorder)
                {
                    floorTileList = currentZoneSettings.ExteriorFloorSegements;

                    List<ItemDisplay> borderAisleDisplayList = null;
                    var borderAisleDisplayPosition = position;
                    var borderAisleDisplayRotation = 0f;

                    if (!neighbourT.IsPopulated)
                    {
                        if (!neighbourL.IsPopulated)
                        {
                            borderAisleDisplayList = currentZoneSettings.ExteriorCornerZoneBorders;
                        }
                        else if (!neighbourR.IsPopulated)
                        {
                            borderAisleDisplayList = currentZoneSettings.ExteriorCornerZoneBorders;

                            borderAisleDisplayRotation = 90f;
                            borderAisleDisplayPosition.x += currentLevelSettings.AisleCellSize.x;
                        }
                        else
                        {
                            borderAisleDisplayList = neighbourL.neighbourT.IsPopulated || neighbourR.neighbourT.IsPopulated ? currentZoneSettings.ExteriorEdgeZoneBorders : currentZoneSettings.ExteriorMidZoneBorders;
                        }
                    }
                    else if (!neighbourB.IsPopulated)
                    {
                        if (!neighbourL.IsPopulated)
                        {
                            borderAisleDisplayList = currentZoneSettings.ExteriorCornerZoneBorders;
                            borderAisleDisplayRotation = 270f;
                            borderAisleDisplayPosition.z -= currentLevelSettings.AisleCellSize.y;
                        }
                        else if (!neighbourR.IsPopulated)
                        {
                            borderAisleDisplayList = currentZoneSettings.ExteriorCornerZoneBorders;
                            borderAisleDisplayRotation = 180f;
                            borderAisleDisplayPosition.x += currentLevelSettings.AisleCellSize.x;
                            borderAisleDisplayPosition.z -= currentLevelSettings.AisleCellSize.y;
                        }
                        else
                        {
                            borderAisleDisplayList = neighbourL.neighbourB.IsPopulated || neighbourR.neighbourB.IsPopulated ? currentZoneSettings.ExteriorEdgeZoneBorders : currentZoneSettings.ExteriorMidZoneBorders;
                            borderAisleDisplayPosition.x += currentLevelSettings.AisleCellSize.x;
                            borderAisleDisplayPosition.z -= currentLevelSettings.AisleCellSize.y;
                            borderAisleDisplayRotation = 180f;
                        }
                    }
                    else
                    {
                        floorTileList = currentZoneSettings.FloorSegements;

                        if (neighbourT.IsPopulated && neighbourB.IsPopulated && neighbourL.IsPopulated && neighbourR.IsPopulated)
                        {
                            if (!neighbourT.neighbourL.IsPopulated)
                            {
                                var borderItemDisplay = Object.Instantiate(currentZoneSettings.InteriorCornerZoneBorders.PickRandomItem(), parentTransform);
                                borderItemDisplay.transform.localPosition = position;
                            }
                            
                            if (!neighbourT.neighbourR.IsPopulated)
                            {
                                var borderItemDisplay = Object.Instantiate(currentZoneSettings.InteriorCornerZoneBorders.PickRandomItem(), parentTransform);
                                borderItemDisplay.transform.localPosition = position + new Vector3(currentLevelSettings.AisleCellSize.x, 0f, 0f);
                                borderItemDisplay.transform.Rotate(0f, 90f, 0f);
                            }
                            
                            if (!neighbourB.neighbourL.IsPopulated)
                            {
                                var borderItemDisplay = Object.Instantiate(currentZoneSettings.InteriorCornerZoneBorders.PickRandomItem(), parentTransform);
                                borderItemDisplay.transform.localPosition = position + new Vector3(0f, 0f, -currentLevelSettings.AisleCellSize.y);
                                borderItemDisplay.transform.Rotate(0f, 270f, 0f);
                            }
                            
                            if (!neighbourB.neighbourR.IsPopulated)
                            {
                                var borderItemDisplay = Object.Instantiate(currentZoneSettings.InteriorCornerZoneBorders.PickRandomItem(), parentTransform);
                                borderItemDisplay.transform.localPosition = position + new Vector3(currentLevelSettings.AisleCellSize.x, 0f, -currentLevelSettings.AisleCellSize.y);
                                borderItemDisplay.transform.Rotate(0f, 180f, 0f);
                            }
                        }
                    }

                    if (borderAisleDisplayList != null)
                    {
                        var borderItemDisplay = Object.Instantiate(borderAisleDisplayList.PickRandomItem(), parentTransform);
                        borderItemDisplay.transform.localPosition = borderAisleDisplayPosition;
                        borderItemDisplay.transform.Rotate(0f, borderAisleDisplayRotation, 0f);
                    }
                }

                var floorObj = Object.Instantiate(floorTileList.PickRandomItem(), parentTransform);
                floorObj.transform.localPosition = position;

                if (leftAisleSettings != null)
                {
                    var leftAisleDisplay = Object.Instantiate(leftAisleSettings.itemDisplays.PickRandomItem(), parentTransform);
                    leftAisleDisplay.transform.localPosition = position;
                    leftAisleDisplay.Populate(parentTransform, leftAisleShoppingItems);
                }

                if (rightAisleSettings != null)
                {
                    position.x += currentLevelSettings.AisleCellSize.x;
                    position.z -= currentLevelSettings.AisleCellSize.y;

                    var rightAisleDisplay = Object.Instantiate(rightAisleSettings.itemDisplays.PickRandomItem(), parentTransform);
                    rightAisleDisplay.transform.localPosition = position;
                    rightAisleDisplay.transform.Rotate(0f, 180f, 0f);
                    rightAisleDisplay.Populate(parentTransform, rightAisleShoppingItems);
                }
            }

            public void UpdateAisles(LevelSettings currentLevelSettings)
            {
                if (!IsPopulated)
                {
                    return;
                }

                if (IsZonedBorder)
                {
                    //populate with exterior walls;

                    return;
                }

                if (IsZoned)
                {
                    if (!neighbourL.neighbourL.IsZonedBorder)
                    {
                        leftAisleSettings = neighbourL.IsPopulated ? zoneSettings.GetMidAisleSettings() : zoneSettings.GetExteriorBorderAisleSettings();
                        leftAisleShoppingItems = zoneSettings.ShoppingItems;
                    }

                    if (!neighbourR.neighbourR.IsZonedBorder)
                    {
                        rightAisleSettings = neighbourR.IsPopulated ? zoneSettings.GetMidAisleSettings() : zoneSettings.GetExteriorBorderAisleSettings();
                        rightAisleShoppingItems = zoneSettings.ShoppingItems;
                    }

                    return;
                }

                if (neighbourL.IsZoned && !neighbourL.isBorder && !neighbourR.IsZonedBorder)
                {
                    leftAisleSettings = neighbourL.zoneSettings.GetInteriorBorderAisleSettings();
                    leftAisleShoppingItems = neighbourL.zoneSettings.ShoppingItems;
                }
                else if (!neighbourL.IsPopulated)
                {
                    leftAisleSettings = currentLevelSettings.borderZoneSettings.GetExteriorBorderAisleSettings();
                    leftAisleShoppingItems = currentLevelSettings.borderZoneSettings.ShoppingItems;
                }

                if (neighbourR.IsZoned && !neighbourR.isBorder && !neighbourL.IsZonedBorder)
                {
                    rightAisleSettings = neighbourR.zoneSettings.GetInteriorBorderAisleSettings();
                    rightAisleShoppingItems = neighbourR.zoneSettings.ShoppingItems;
                }
                else if (!neighbourR.IsPopulated)
                {
                    rightAisleSettings = currentLevelSettings.borderZoneSettings.GetExteriorBorderAisleSettings();
                    rightAisleShoppingItems = currentLevelSettings.borderZoneSettings.ShoppingItems;
                }
            }
        }

        [FormerlySerializedAs("currentSettings")] public LevelSettings currentLevelSettings;
        [SerializeField] private bool clearExisting;

        private void OnEnable()
        {
            if (currentLevelSettings == null)
            {
                return;
            }

            if (clearExisting)
            {
                for (var i = transform.childCount - 1; i >= 0; i--)
                {
                    Destroy(transform.GetChild(i).gameObject);
                }
            }

            var rowData = new List<RowData>(currentLevelSettings.RowSettings.Count);
            var levelDimensions = new Vector2Int(0, 1);
            var rowCount = currentLevelSettings.RowSettings.Count;

            for (var index = 0; index < rowCount; index++)
            {
                currentLevelSettings.RowSettings[index].ResetInstance();
                var currentRow = new RowData(currentLevelSettings.RowSettings[index], index == 0 || index == rowCount - 1);
                rowData.Add(currentRow);
                levelDimensions.x = Math.Max(levelDimensions.x, currentRow.rowWidth);
                levelDimensions.y += currentRow.aisleLength + 1;
            }

            var cells = new CellData[levelDimensions.x, levelDimensions.y];

            for (var y = 0; y < levelDimensions.y; y++)
            {
                for (var x = 0; x < levelDimensions.x; x++)
                {
                    cells[x, y] = new CellData(x, y);

                    if (x > 0)
                    {
                        cells[x, y].neighbourL = cells[x - 1, y];
                        cells[x - 1, y].neighbourR = cells[x, y];
                    }

                    if (y > 0)
                    {
                        cells[x, y].neighbourB = cells[x, y - 1];
                        cells[x, y - 1].neighbourT = cells[x, y];
                    }
                }
            }

            var lastRowMinCell = Vector2Int.zero;
            var lastRowMaxCell = new Vector2Int(levelDimensions.x, 0);

            GenerateRow(rowData[0], levelDimensions, ref lastRowMinCell, ref lastRowMaxCell, ref cells, true, false);

            for (var i = 1; i < rowCount; i++)
            {
                GenerateRow(rowData[i], levelDimensions, ref lastRowMinCell, ref lastRowMaxCell, ref cells, false, i < rowCount - 1);
            }

            foreach (var cell in cells)
            {
                cell.UpdateVerticalBorders();
            }

            foreach (var cell in cells)
            {
                cell.UpdateHorizontalBorders();
            }

            foreach (var cell in cells)
            {
                cell.UpdateAisles(currentLevelSettings);
            }

            foreach (var cell in cells)
            {
                cell.Instantiate(currentLevelSettings, transform);
            }
        }

        private void GenerateRow(RowData rowData, Vector2Int levelDimensions, ref Vector2Int lastRowMinCell, ref Vector2Int lastRowMaxCell, ref CellData[,] cells, bool isTopRow, bool isBottomRow)
        {
            var rowOffsetX = lastRowMinCell.x;
            var previousRowDimensions = lastRowMaxCell - lastRowMinCell;

            if (rowData.rowWidth > previousRowDimensions.x || rowData.rowWidth < previousRowDimensions.x)
            {
                rowOffsetX -= Random.Range(0, rowData.rowWidth - previousRowDimensions.x);

                if (rowOffsetX < 0)
                {
                    rowOffsetX = 0;
                }
                else if (rowOffsetX + rowData.rowWidth >= levelDimensions.x)
                {
                    rowOffsetX = levelDimensions.x - rowData.rowWidth;
                }
            }

            lastRowMinCell = new Vector2Int(rowOffsetX, lastRowMaxCell.y + 1);
            lastRowMaxCell = lastRowMinCell + new Vector2Int(rowData.rowWidth, rowData.aisleLength);

            var zoneOffset = lastRowMinCell;

            for (var i = 0; i < rowData.zones.Length; i++)
            {
                var zoneSettings = rowData.zones[i];
                var zoneDimensions = rowData.zoneDimensions[i];

                if (isTopRow)
                {
                    zoneOffset.y = lastRowMaxCell.y - zoneDimensions.y;
                }

                for (var y = 0; y < zoneDimensions.y; y++)
                {
                    var isZoneTop = y == zoneDimensions.y - 1;
                    var isZoneBottom = y == 0;

                    for (var x = 0; x < zoneDimensions.x; x++)
                    {
                        cells[x + zoneOffset.x, y + zoneOffset.y].SetZoneData(zoneSettings, isZoneTop, isZoneBottom, x == 0, x == zoneDimensions.x - 1);
                    }
                }

                zoneOffset.x += zoneDimensions.x + 1;
            }
        }
    }
}