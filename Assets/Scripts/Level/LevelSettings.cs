using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Level
{
    [CreateAssetMenu(fileName = "LevelSettings", menuName = "Level/Level Settings")]
    public class LevelSettings : ScriptableObject
    {
        public Vector2 AisleCellSize = new Vector2(1f, 0.5f);
        // public List<GameObject> BorderFloorTiles;

        public List<StoreRowSettings> RowSettings;

        public StoreZoneSettings borderZoneSettings;
    }
}
