using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Level
{
    public class ItemDisplay : MonoBehaviour
    {
        
        [Serializable]
        public class ItemPositionCollection
        {
            public ShoppingItem.ItemType itemType;
            public List<Transform> positions;
            [Range(0f, 1f)]
            public float itemSpawnProbability = 1f;

            public Vector3 maxPositionOffset = Vector3.zero;
            public Vector3 maxRotationOffset = Vector3.zero;
        }
        
        public List<ItemPositionCollection> itemSpawnPoints;

        private List<ShoppingItem> currentItems = new List<ShoppingItem>();
        
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void Populate(Transform itemParent, Dictionary<ShoppingItem.ItemType, List<ShoppingItem>> potentialItems)
        {
            foreach (var positionCollection in itemSpawnPoints)
            {
                if (!potentialItems.TryGetValue(positionCollection.itemType, out var groupItems))
                {
                    continue;
                }

                var item = groupItems[Random.Range(0, groupItems.Count)];

                foreach (var itemPosition in positionCollection.positions)
                {
                    if (Random.value > positionCollection.itemSpawnProbability)
                    {
                        continue;
                    }

                    var posOffset = Util.GetRandomOffsetVector(positionCollection.maxPositionOffset);
                    var rotOffset = Quaternion.Euler(Util.GetRandomOffsetVector(positionCollection.maxRotationOffset));

                    var itemInstance = Instantiate(item, itemParent);
                    itemInstance.transform.position = itemPosition.position + posOffset;
                    itemInstance.transform.rotation = itemPosition.rotation * rotOffset;
                    
                    currentItems.Add(itemInstance);
                }
            }
        }
    }
}
