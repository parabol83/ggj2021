﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Level
{
    [CreateAssetMenu(fileName = "StoreZoneSettings", menuName = "Level/Store Zone Settings")]
    public class StoreZoneSettings : ScriptableObject
    {
        public List<AisleSettings> InteriorBorderAisles;
        public List<AisleSettings> MidAisles;
        public List<AisleSettings> ExteriorBorderAisles;

        public List<ItemDisplay> InteriorCornerZoneBorders;
        public List<ItemDisplay> ExteriorCornerZoneBorders;
        public List<ItemDisplay> ExteriorEdgeZoneBorders;
        public List<ItemDisplay> ExteriorMidZoneBorders;
        
        [FormerlySerializedAs("TopExteriorFloorSegements")] public List<GameObject> ExteriorFloorSegements;
        public List<GameObject> FloorSegements;
        
        public int MinAisleCount = 3;
        public int MaxAisleCount = 5;
        public int MinAisleLength = 3;
        public int MaxAisleLength = 8;
        
        [SerializeField] private List<ShoppingItem> shoppingItems;

        private Dictionary<ShoppingItem.ItemType, List<ShoppingItem>> shoppingItemDictionary;

        public void ResetInstance()
        {
            shoppingItemDictionary = null;
        }

        public Dictionary<ShoppingItem.ItemType, List<ShoppingItem>> ShoppingItems
        {
            get
            {
                if (shoppingItemDictionary != null)
                {
                    return shoppingItemDictionary;
                }
                
                shoppingItemDictionary = new Dictionary<ShoppingItem.ItemType, List<ShoppingItem>>();

                foreach (var shoppingItem in shoppingItems)
                {
                    if (!shoppingItemDictionary.TryGetValue(shoppingItem.itemType, out var items))
                    {
                        items = new List<ShoppingItem>();
                        shoppingItemDictionary.Add(shoppingItem.itemType, items);
                    }
                    
                    items.Add(shoppingItem);
                }

                return shoppingItemDictionary;
            }
        }

        public AisleSettings GetInteriorBorderAisleSettings()
        {
            if (InteriorBorderAisles == null || InteriorBorderAisles.Count <= 0)
            {
                return GetMidAisleSettings();
            }
                
            return InteriorBorderAisles.PickRandomItem();
        }
            
        public AisleSettings GetMidAisleSettings()
        {
            return MidAisles.PickRandomItem();
        }
            
        public AisleSettings GetExteriorBorderAisleSettings()
        {
            if (ExteriorBorderAisles == null || ExteriorBorderAisles.Count <= 0)
            {
                return GetMidAisleSettings();
            }
                
            return ExteriorBorderAisles.PickRandomItem();
        }

        public Vector2Int GetDimensions()
        {
            return new Vector2Int(Random.Range(MinAisleCount, MaxAisleCount), Random.Range(MinAisleLength, MaxAisleLength));
        }
    }
}