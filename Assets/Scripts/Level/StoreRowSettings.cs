﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Level
{
    [CreateAssetMenu(fileName = "StoreRowSettings", menuName = "Level/Store Row Settings")]
    public class StoreRowSettings : ScriptableObject
    {
        [Serializable]
        public class RowSegmentSettings
        {
            public StoreZoneSettings[] zones;
            public int minZoneCount;
            public int maxZoneCount;
        }

        public List<RowSegmentSettings> rowSegments;
        public bool randomizeSegmentOrder;
        public int minAisleLength;
        public int maxAisleLenght;
        public bool allowCustomZoneAisleLength;

        public void ResetInstance()
        {
            foreach (var rowSegment in rowSegments)
            {
                foreach (var zone in rowSegment.zones)
                {
                    zone.ResetInstance();
                }
            }
        }

        public StoreZoneSettings[] GetZoneList()
        {
            var segments = new List<RowSegmentSettings>(rowSegments);

            if (randomizeSegmentOrder)
            {
                segments.Shuffle();
            }

            var retVal = new List<StoreZoneSettings>();

            foreach (var segment in segments)
            {
                var zoneCount = Random.Range(segment.minZoneCount, segment.maxZoneCount);

                for (var i = 0; i < zoneCount; i++)
                {
                    retVal.Add(segment.zones.PickRandomItem());
                }
            }

            return retVal.ToArray();
        }
    }
}