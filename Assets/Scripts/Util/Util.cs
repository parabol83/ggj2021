using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public static void Shuffle<T>(this IList<T> list)
    {
        var i = list.Count;
        while (i > 1)
        {
            i--;
            var newIndex = Random.Range(0, i + 1);
            var temp = list[newIndex];
            list[newIndex] = list[i];
            list[i] = temp;
        }
    }

    public static T PickRandomItem<T>(this IList<T> list)
    {
        if (list.Count <= 0)
        {
            return default;
        }

        var index = Random.Range(0, list.Count);
        return list[index];
    }

    public static Vector3 GetRandomOffsetVector(Vector3 maxRange)
    {
        return new Vector3(
            Random.Range(-maxRange.x, maxRange.x),
            Random.Range(-maxRange.y, maxRange.y),
            Random.Range(-maxRange.z, maxRange.z)
        );
    }
}
