using System;
using System.Collections;
using UnityEngine;

namespace Characters
{
    [RequireComponent(typeof(Animator))]
    public class CharacterAnimationController : MonoBehaviour
    {
        protected Animator _animator;

        public bool leftHandIKActive = false;
        public bool rightHandIKActive = false;

        public Transform leftHandTarget = null;
        public Transform rightHandTarget = null;
        public Transform lookTarget = null;

        public float movementSpeed;
        private static readonly int MovementSpeed = Animator.StringToHash("MovementSpeed");

        public int CurrentAisle = 0;

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void OnEnable()
        {
        }

        private void OnAnimatorIK(int layerIndex)
        {
            if (_animator)
            {
                _animator.SetFloat(MovementSpeed, movementSpeed);
                
                // If the IK is active, set the position and rotation directly to the goal.

                if (lookTarget != null)
                {
                    _animator.SetLookAtWeight(1);
                    _animator.SetLookAtPosition(lookTarget.position);
                }
                else
                {
                    _animator.SetLookAtWeight(0);
                }

                if (leftHandIKActive && leftHandTarget != null)
                {
                    _animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                    _animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                    _animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.position);
                    _animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandTarget.rotation);
                }
                else
                {
                    _animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
                    _animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
                }
                
                if (rightHandIKActive && rightHandTarget != null)
                {
                    _animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    _animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                    _animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);
                    _animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandTarget.rotation);
                }
                else
                {
                    _animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                    _animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                }
            }
        }
    }
}
