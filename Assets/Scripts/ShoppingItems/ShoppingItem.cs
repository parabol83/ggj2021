using System;
using System.Collections;
using System.Collections.Generic;
using Level;
using UnityEngine;

public class ShoppingItem : MonoBehaviour
{
    public enum ItemType
    {
        Small,
        Medium,
        Large,
    }
    
    private ItemDisplay parentDisplay;
    public string itemName;
    public ItemType itemType;
    public int price;
    public string itemDescription;
}
